//function getResult(byName){
//    let addr = $("#address").val();
//    $.ajax({
//    url:"/location/check.htm", type: "GET", dataType: 'json', data : {addr, byName},
//    success: function (data){
//        let inOut = data.okToDrive ? "within" : "outside";
//        $("#result").html(`You are ${inOut} our service area. Distance is:  ${data.miles}`);
//
//    }, error : function (error){
//        console.log(error);
//    }
//    });
//}
function init(){
$('#bookBtn').hide();
}
let autocomp;
function initMap() {
    const center = { lat: 42.25546941374695, lng: -83.66480036014951 };
    // Create a bounding box with sides ~10km away from the center point
    const defaultBounds = {
      north: center.lat + 0.2,
      south: center.lat - 0.2,
      east: center.lng + 0.2,
      west: center.lng - 0.2,
    };
    let address = document.getElementById("address");
    const options = {
      bounds: defaultBounds,
      componentRestrictions: { country: "us" },
      fields: ["address_components", "geometry", "icon", "name"],
      origin: center,
      strictBounds: true,
    };
    autocomp = new google.maps.places.Autocomplete(address, options);
    autocomp.addListener('place_changed', placeChanged);
}
function placeChanged(){
    let place = autocomp.getPlace();
    if (! place) {
        $("#result")
            .html(`Not initialized`)
            .removeClass("ok not-ok")
            .addClass("not-ok");
        return;
    }
    let inOut ;
    let resClass ;
    if (place.address_components) {
        inOut = "within";
        resClass = "ok";
        $('#bookBtn').show();
    } else {
        inOut = "outside";
        resClass = "not-ok";
        $('#bookBtn').hide();
    }


    $("#result")
        .html(`You are ${inOut} our service area.`)
        .removeClass("ok not-ok")
        .addClass(resClass);
    console.log("in placeChanged inOut is" + inOut);
}
$(document).ready(init);