function myBlogUpload(){
    let content=tinyMCE.get('content').getContent();
    let blog={
        //id: $("#id").val(),//
        author: $("#author").val(),
        title: $("#title").val(),
        date: $("#date").val(),
        imageURL: $("#imageURL").val(),
        content: content,
        authorImg: $("#authorImg").val(),
        heading: $("#heading").val(),
        subHeading: $("#subHeading").val(),
        lead: $("#lead").val()
    };

    $.ajax({url: '/blog/update.htm',method:'post',dataType:'json',
     data: JSON.stringify(blog),
        contentType: 'application/json; charset=utf-8',
        success : function(result){
            alert('Uploaded successfully with id ' + result.id);
        },  error : function(error){
            alert('Failed uploaded');
        }
    });
}