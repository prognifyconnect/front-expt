let blogs;
let blogsShown = 0;
$(document).ready(function(){
    showNextThree(true);
});
function showNextThree(force){
//Load from Server//
    $.ajax({
        url:"/blog/summary.htm", dataType:"JSON", data:{force:force},
        success:function(result){
            if (result.length == 0) {
            alert ("No more blogs to be loaded");
            $("#showMoreBtn").hide();
            return;
            }
                for (let i=0;i<result.length;i++){
                displayBlog(result[i]);
                }
            //Show first three//

        }, error:function(err){
            alert("Some error occurred");
        }
    });
}
function displayBlog(blog){
    let template=$(".blog-template").clone();
    template.find(".blog-title").html(blog.title);
    template.find(".blog-date").html(myDateFunction(blog.date));
    Modernizr.on('webp', function(result) {
      if (result) {
        // supported
        let myImageSrc = blog.imageURL.replace(/\.[^.]+/, ".webp");
        template.find(".blog-image").attr("src", myImageSrc);
      } else {
        // not-supported
        template.find(".blog-image").attr("src", blog.imageURL);
      }
    });
    template.find("blog-read-more-link").attr("href", "/blog/detail.htm    ?id="+ blog.id);
    template.attr("onclick", "window.open('/blog/detail.htm?id="+ blog.id+"');");
    template.removeClass("blog-template");
    $(".blog-holder").append(template);
}
function myDateFunction(blogDate) {
  var trimDate = new Date(blogDate);
  var newDate = trimDate.toLocaleDateString();
  return newDate;
}
//Auth Code//
function myAuth(){
    var ui = new firebaseui.auth.AuthUI(firebase.auth());
    ui.start('#firebaseui-auth-container', {
        signInOptions: [
          // List of OAuth providers supported.
          firebase.auth.GoogleAuthProvider.PROVIDER_ID,
          firebase.auth.FacebookAuthProvider.PROVIDER_ID,
     //     firebase.auth.TwitterAuthProvider.PROVIDER_ID,
       //   firebase.auth.GithubAuthProvider.PROVIDER_ID
       firebase.auth.PhoneAuthProvider.PROVIDER_ID,
        ],
        // Other config options...
      });
    }
